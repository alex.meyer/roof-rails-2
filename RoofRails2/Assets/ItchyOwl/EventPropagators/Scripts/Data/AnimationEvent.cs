﻿using UnityEngine.Events;
using System;

namespace ItchyOwl.Propagators
{
    [Serializable]
    public class AnimationEvent : UnityEvent<string> { }
}
