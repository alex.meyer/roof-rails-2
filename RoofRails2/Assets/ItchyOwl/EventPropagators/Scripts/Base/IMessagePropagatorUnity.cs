﻿namespace ItchyOwl.Propagators
{
    /// <summary>
    /// Common interface for all components that propagate Unity callbacks via Unity Events.
    /// </summary>
    public interface IMessagePropagatorUnity : IMessagePropagator { }
}
