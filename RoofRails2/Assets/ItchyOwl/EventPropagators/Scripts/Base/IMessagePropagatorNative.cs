﻿namespace ItchyOwl.Propagators
{
    /// <summary>
    /// Common interface for all components that propagate Unity callbacks via native C# events.
    /// </summary>
    public interface IMessagePropagatorNative : IMessagePropagator { }
}
