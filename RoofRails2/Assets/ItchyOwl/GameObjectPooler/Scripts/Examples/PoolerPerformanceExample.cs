﻿using UnityEngine;
using UnityEngine.UI;

namespace ItchyOwl.ObjectPooling.Examples
{
    public class PoolerPerformanceExample : MonoBehaviour
    {
        public Text objectCountText;
        public Toggle toggle;
        public Slider slider;
        public GameObject prefab;
        public int objectsPerFrame;
        [Tooltip("Try to enable and see how the performance drops.")]
        public bool setParent;

        private bool enablePooling;
        private GameObjectPool pool;
        private string originalText;

        private void Start()
        {
            pool = prefab.GetPool(setParent: setParent);
            prefab.Prefabricate(objectsPerFrame);
            enablePooling = toggle.isOn;
            toggle.onValueChanged.AddListener(v => enablePooling = v);
            slider.value = objectsPerFrame;
            slider.onValueChanged.AddListener(v => objectsPerFrame = (int)v);
            originalText = objectCountText.text;
        }

        private void Update()
        {
            objectCountText.text = originalText + objectsPerFrame;
            if (enablePooling)
            {
                // This provides best performance, because we skip the dictionary checks.
                for (int i = 0; i < objectsPerFrame; i++)
                {
                    var instance = pool.Pop();
                    pool.Inactivate(instance);
                }
                // This provides ok performance for most use cases.
                // It's preferable because there are some checks for duplicates and because it does not require the reference to the pool.
                //for (int i = 0; i < objectsPerFrame; i++)
                //{
                //    var instance = prefab.Spawn();
                //    instance.Recycle();
                //}
            }
            else
            {
                // Insufficent performance for in many cases
                for (int i = 0; i < objectsPerFrame; i++)
                {
                    var instance = Instantiate(prefab);
                    Destroy(instance);
                }
            }
        }
    }
}
