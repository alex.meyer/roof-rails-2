﻿using System.Collections;
using UnityEngine;

namespace ItchyOwl.ObjectPooling.Examples
{
    public class PoolerBasicUsageExample : MonoBehaviour
    {
        public GameObject prefab;
        public int bufferSize = 10;
        public float lifeTime = 1;

        private void Start()
        {
            if (bufferSize < 0) { bufferSize = 0; }
            // Subscribing to the pool events. Pools are automatically created when they are needed, but you can also create a pool by calling GetPool() extension method.
            GameObjectPool pool = prefab.GetPool();
            pool.ObjectCreated += go => Debug.LogFormat("{0} created", go.name);
            pool.ObjectDestroyed += go => Debug.LogFormat("{0} destroyed", go.name);
            pool.ObjectActivated += go => Debug.LogFormat("{0} activated", go.name);
            pool.ObjectInactivated += go => Debug.LogFormat("{0} inactivated", go.name);
            // Prefabricate objects to speed up spawning, if multiple objects are spawned at once (for example a shotgun with multiple projectiles).
            prefab.Prefabricate(bufferSize * 2);
            // Releasing pooled objects. You can also use instance.ReleaseInactives() extension method.
            prefab.ReleaseInactivesOfPrefab(bufferSize);
            StartCoroutine(SpawnAndRecycle());
        }

        private IEnumerator SpawnAndRecycle()
        {
            var wait = new WaitForSeconds(lifeTime);
            while (true)
            {
                // Spawning. Replace Instantiate(prefab) with this extension method.
                var instance = prefab.Spawn();
                yield return wait;
                // Recycles the instance back to the object pool. Replace Destroy(gameObject) with this extension method.
                instance.Recycle(go => ResetObject());
                yield return null;
            }
        }

        private void ResetObject()
        {
            // Resetting the object can be done either in the callback, inside OnEnable/OnDisable methods, or via subscribing to the pool's events.
        }
    }
}
