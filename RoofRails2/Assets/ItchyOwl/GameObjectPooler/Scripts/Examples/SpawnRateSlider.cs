﻿using UnityEngine;
using UnityEngine.UI;
using ItchyOwl.Auxiliary;

namespace ItchyOwl.ObjectPooling.Examples
{
    [RequireComponent(typeof(Slider))]
    public class SpawnRateSlider : MonoBehaviour
    {
        public PrefabSpawner spawner;

        private void Start()
        {
            var slider = GetComponent<Slider>();
            slider.value = spawner.threshold;
            slider.onValueChanged.AddListener(v => spawner.threshold = v);
        }
    }
}
