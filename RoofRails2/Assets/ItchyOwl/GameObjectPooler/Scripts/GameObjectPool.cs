﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace ItchyOwl.ObjectPooling
{
    /// <summary>
    /// This class should be managed my GameObjectPooler. Don't add it manually in the scene.
    /// </summary>
    public class GameObjectPool : MonoBehaviour
    {
        public GameObject prefab;
        /// <summary>
        /// Setting the transform parent is relatively expensive operation. For best performance, don't assign.
        /// </summary>
        public Transform activeParent;
        /// <summary>
        /// Setting the transform parent is relatively expensive operation. For best performance, don't assign.
        /// </summary>
        public Transform inactiveParent;

        private List<GameObject> _activeInstances = new List<GameObject>();
        private Stack<GameObject> _inactiveInstances = new Stack<GameObject>();

        public IEnumerable<GameObject> ActiveInstances { get { return _activeInstances; } }
        public IEnumerable<GameObject> InactiveInstances { get { return _inactiveInstances; } }

        public int ActiveInstanceCount { get { return _activeInstances.Count; } }
        public int InactiveInstanceCount { get { return _inactiveInstances.Count; } }

        public event Action<GameObject> ObjectActivated;
        public event Action<GameObject> ObjectInactivated;
        public event Action<GameObject> ObjectCreated;
        public event Action<GameObject> ObjectDestroyed;

        /// <summary>
        /// Always creates a new instance.
        /// </summary>
        public GameObject CreateNewInstance(bool startActive)
        {
            var instance = Instantiate(prefab);
            if (ObjectCreated != null)
            {
                ObjectCreated(instance);
            }
            if (startActive)
            {
                Activate(instance);
            }
            else
            {
                Inactivate(instance);
            }
            return instance;
        }

        /// <summary>
        /// Returns a new instance, which is either instantiated or recycled.
        /// </summary>
        public GameObject Pop()
        {
            return _inactiveInstances.Count > 0 ? PopAndActivate() : CreateNewInstance(startActive: true);
        }

        /// <summary>
        /// Inactivates the object.
        /// Note that for performance reasons there is no check whether or not the object is already inactivated. 
        /// </summary>
        public void Inactivate(GameObject go, Action<GameObject> callback = null)
        {
            _activeInstances.Remove(go);
            InactivateInternal(go, callback);
        }

        public void InactivateAll(Action<GameObject> callbackPerObject = null)
        {
            int count = _activeInstances.Count;
            // Inactivates objects starting from the last and ending to the first.
            int lastIndex = count - 1;
            for (int i = lastIndex; i >= 0; i--)
            {
                var instance = _activeInstances[i];
                _activeInstances.RemoveAt(i);
                InactivateInternal(instance, callbackPerObject);
            }
        }

        /// <summary>
        /// Destroys inactive instances.
        /// If the count is left zero, all the inactives are destroyed.
        /// </summary>
        public int ReleaseInactives(int count = 0)
        {
            if (count > _inactiveInstances.Count || count == 0)
            {
                count = _inactiveInstances.Count;
            }
            int counter = 0;
            while (counter < count)
            {
                var instance = _inactiveInstances.Pop();
                if (ObjectDestroyed != null)
                {
                    ObjectDestroyed(instance);
                }
                Destroy(instance);
                counter++;
            }
            return count;
        }

        /// <summary>
        /// NOTE: Does not automatically remove the item from the activeInstances. That should be done before calling this method!
        /// </summary>
        private void InactivateInternal(GameObject go, Action<GameObject> callback = null)
        {
            if (inactiveParent != null)
            {
                go.transform.SetParent(inactiveParent, go.transform as RectTransform == null);
            }
            go.SetActive(false);
            // As an optimization, there is no check here whether the stack already contains the item.
            _inactiveInstances.Push(go);
            if (callback != null) { callback(go); }
            if (ObjectInactivated != null)
            {
                ObjectInactivated(go);
            }
        }

        private GameObject PopAndActivate()
        {
            var instance = _inactiveInstances.Pop();
            Activate(instance);
            return instance;
        }

        private void Activate(GameObject go)
        {
            if (activeParent != null)
            {
                go.transform.SetParent(activeParent, go.transform as RectTransform == null);
            }
            // As an optimization, there is no check here whether the list already contains the item.
            // There should not be duplicates, because this method is called internally only after either instantiation or popping from the inactives stack.
            _activeInstances.Add(go);
            go.SetActive(true);
            if (ObjectActivated != null)
            {
                ObjectActivated(go);
            }
        }
    }
}