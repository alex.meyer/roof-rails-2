v.1.0

Thank you for purchasing GameObjectPooler and for supporting a fellow developer :)

If you have any questions, don't hesitate to mail me at contact@itchyowl.com!

INSTRUCTIONS
The pooler is based on an entirely static helper class called GameObjectPooler. This helper class automatically manages the pools for you, but you can also use the pools directly for full control.

Have a look at the demo scenes found in ItchyOwl/GameObjectPooler/Scenes/ folder. GameObjectPoolerDemo shows how to use the system. GameObjectPoolerPerformanceDemo demonstrates the performance differences between pooling and not pooling. It also addresses some tweaks that you can use to further improve your performance.

All the scripts can be found in ItchyOwl/Scripts/. In the root, you'll find two scripts: GameObjectPooler.cs and GameObjectPool.cs. Together these two form the GameObject Pooler system. There are also some auxiliary files that can be found in ItchyOwl/_Common/ folder.

NOTE
- GameObject Pooler ships with Event Propagators included. For instructions, see ItchyOwl/EventPropagators/ folder.
- If you also use Collision Propagators, please import it BEFORE installing GameObjectPooler, because Collision Propagators uses a stripped-down version of PrefabSpawner script.