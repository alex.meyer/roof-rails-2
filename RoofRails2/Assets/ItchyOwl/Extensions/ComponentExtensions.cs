using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using ItchyOwl.DataManagement;

namespace ItchyOwl.Extensions
{
    public static class ComponentExtensions
    {
        /// <summary>
        /// Copies all fields and properties of the source to the destination.
        /// Usage: myComp.CopyValues(someOtherComponent).
        /// Note: In editor scripts, use EditorUtility.CopySerialized(source, target) instead.
        /// Note that this method may cause unecessary material instances to be instantiated, where we would like to use a shared materials instead.
        /// </summary>
        public static T CopyValuesOf<T>(this Component comp, T source) where T : Component
        {
            return FileManager.CopyValues(source, comp as T);
        }
    }
}

