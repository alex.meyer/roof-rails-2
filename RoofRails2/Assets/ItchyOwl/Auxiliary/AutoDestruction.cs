﻿using System;
using UnityEngine;
using ItchyOwl.ObjectPooling;

namespace ItchyOwl.Auxiliary
{
    public class AutoDestruction : MonoBehaviour
    {
        public bool usePooling;
        [Tooltip("Automatically initializes destruction each time OnEnable is called.")]
        public bool autoInitializeDestruction;
        /// <summary>
        /// Called when the object is recycled back to the object pool. Is not used, if the object is destroyed instead of pooling.
        /// </summary>
        public Action resetCallback;
        [Tooltip("When enabled, the position of the object is stored on Start and reset when the object is pooled. Has no effect is pooling is disabled.")]
        public bool resetPositionWhenPooled;
        [Tooltip("Changing the value after the destruction has been initialized, does not have an effect. However, recalling InitializeDestruction resets the routine properly.")]
        public float lifeTime = Mathf.Infinity;
        public bool IsDestructionInitialized { get; private set; }

        private float timer;

        private void OnEnable()
        {
            if (autoInitializeDestruction)
            {
                InitializeDestruction(lifeTime);
            }
        }

        private void OnDisable()
        {
            if (!usePooling) { return; }
            IsDestructionInitialized = false;
            timer = 0;
            if (resetCallback != null)
            {
                resetCallback();
            }
        }

        public void InitializeDestruction(float time)
        {
            IsDestructionInitialized = true;
            lifeTime = time;
        }

        private void Update()
        {
            if (!IsDestructionInitialized) { return; }
            timer += Time.deltaTime;
            if (timer >= lifeTime)
            {
                Destroy();
            }
        }

        private void Destroy()
        {
            if (usePooling)
            {
                gameObject.Recycle();
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
