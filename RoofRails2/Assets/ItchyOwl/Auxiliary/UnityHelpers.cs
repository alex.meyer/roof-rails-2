﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace ItchyOwl.Auxiliary
{
    public static class UnityHelpers
    {
        public static Vector3 RandomVector3(float min, float max)
        {
            return new Vector3(Random.Range(min, max), Random.Range(min, max), Random.Range(min, max));
        }
    }
}
