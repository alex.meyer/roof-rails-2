using System;
using UnityEngine;

namespace RoofRails2
{
    public class ActivateTapToContinueByGameState : MonoBehaviour
    {
        [SerializeField] private GameStateConfigVariable currentGameStateVariable;

        private void Awake()
        {
            ValidateReferences();
            RegisterLifetimeListeners();
        }

        private void OnDestroy()
        {
            UnregisterLifetimeListeners();
        }

        private void OnEnable()
        {
            GameStateChanged(currentGameStateVariable.Content);
        }

        private void GameStateChanged(GameStateConfig newGameState)
        {
            gameObject.SetActive(newGameState.showTapToContinue);

            // Debug.Log(
            //     $"[TAP] new game state: {newGameState}  caused game object {gameObject.name} active: {(gameObject.activeInHierarchy ? "active" : "inactive")}, show tap to continue: {newGameState.showTapToContinue}");
        }

        private void RegisterLifetimeListeners()
        {
            currentGameStateVariable.RegisterListener(GameStateChanged);
        }

        private void UnregisterLifetimeListeners()
        {
            currentGameStateVariable.UnregisterListener(GameStateChanged);
        }

        private void ValidateReferences()
        {
            Debug.Assert(currentGameStateVariable != null, "currentGameStateVariable != null");
        }

    }
}