using System;
using UnityEngine;

namespace RoofRails2
{
    public class InGameHudController : MonoBehaviour
    {
        [SerializeField] private GameStateConfigVariable currentGameStateVariable;

        private void Awake()
        {
            ValidateReferences();
            RegisterLifetimeListeners();
        }

        private void OnDestroy()
        {
            UnregisterLifetimeListeners();
        }

        private void OnEnable()
        {
            GameStateChanged(currentGameStateVariable.Content);
        }

        private void GameStateChanged(GameStateConfig newGameState)
        {
            gameObject.SetActive(newGameState.showInGameHud);
        }

        private void RegisterLifetimeListeners()
        {
            currentGameStateVariable.RegisterListener(GameStateChanged);
        }

        private void UnregisterLifetimeListeners()
        {
            currentGameStateVariable.UnregisterListener(GameStateChanged);
        }

        private void ValidateReferences()
        {
            Debug.Assert(currentGameStateVariable != null, "currentGameStateVariable != null");
        }
    }
}