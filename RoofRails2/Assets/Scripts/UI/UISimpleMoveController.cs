using System;
using DG.Tweening;
using UnityEngine;

namespace RoofRails2
{
    [RequireComponent(typeof(RectTransform))]
    public class UISimpleMoveController : MonoBehaviour
    {
        [SerializeField] private RectTransform rectTransform;

        [SerializeField] private float duration = 0.5f;
        [SerializeField] private Vector2 target;

        private Sequence m_seq = null;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();

            m_seq = DOTween.Sequence()
                .Append(rectTransform.DOAnchorPos(target, duration))
                .SetEase(Ease.InOutSine)
                .SetLoops(-1, LoopType.Yoyo);
        }

        private void OnDisable()
        {
            m_seq.Kill();
            UnregisterListeners();
        }

        private void RegisterListeners() { }

        private void UnregisterListeners() { }

        private void ValidateReferences()
        {
            rectTransform = GetComponent<RectTransform>();

            Debug.Assert(rectTransform != null, "rectTransform != null");
        }
    }
}