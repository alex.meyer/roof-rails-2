using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RoofRails2
{
    public class FakeLoadingIndicatorController : MonoBehaviour
    {
        [SerializeField] private Image leftIndicator;
        [SerializeField] private Image rightIndicator;

        [SerializeField] private float duration;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            AnimateBars();
        }

        private void AnimateBars()
        {
            DOTween.To(x => leftIndicator.fillAmount = x, 0, 1.0f, duration)
                .OnComplete(LoadFirstLevel);
            DOTween.To(x => rightIndicator.fillAmount = x, 0, 1.0f, duration);
        }

        private void LoadFirstLevel()
        {
            SceneManager.LoadScene("Level 01");
        }

        private void ValidateReferences()
        {
            Debug.Assert(leftIndicator != null, "leftIndicator != null");
            Debug.Assert(rightIndicator != null, "rightIndicator != null");
        }
    }
}