using TMPro;
using UnityEngine;

namespace RoofRails2
{
    public class UIIntAmountViewController : MonoBehaviour
    {
        [Header("Variables")] [SerializeField] private IntVariable intAmountVariable;

        [Header("View Components")]
        [SerializeField] private TextMeshProUGUI intAmountText;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
            IntAmountChanged(intAmountVariable.Content);
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void IntAmountChanged(int val)
        {
            intAmountText.text = $"{val}";
        }

        private void RegisterListeners()
        {
            intAmountVariable.RegisterListener(IntAmountChanged);
        }

        private void UnregisterListeners()
        {
            intAmountVariable.UnregisterListener(IntAmountChanged);
        }

        private void ValidateReferences()
        {
            Debug.Assert(intAmountVariable != null, "diamondsAmountVariable != null");
            Debug.Assert(intAmountText != null, "diamondsAmountText != null");
        }

    }
}