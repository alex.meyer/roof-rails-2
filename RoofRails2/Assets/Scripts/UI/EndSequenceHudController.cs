using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace RoofRails2
{
    public class EndSequenceHudController : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private GameStateConfigVariable currentGameStateVariable;
        [SerializeField] private IntVariable barCounterVariable;

        [Header("Events")]
        [SerializeField] private EventSO playParticleEffectEvent;
        [SerializeField] private FinishConfigEvent finishConfigEvent;
        [SerializeField] private EventSO endSequenceFinishedEvent;

        [Header("View Components")]
        [SerializeField] private Animator animator;
        [SerializeField] private TextMeshProUGUI barCounterText;
        [SerializeField] private TextMeshProUGUI factorText;
        [SerializeField] private TextMeshProUGUI sumText;

        [Header("Dynamic Data")]
        [SerializeField] private float factor;

        private void Awake()
        {
            ValidateReferences();
            RegisterLifetimeListeners();
        }

        private void OnDestroy()
        {
            UnregisterLifetimeListeners();
        }

        private void BarCounterChanged(int newVal)
        {
            RenderView();
        }

        private void OnEnable()
        {
            GameStateChanged(currentGameStateVariable.Content);
            BarCounterChanged(barCounterVariable.Content);
        }

        private void RenderView()
        {
            barCounterText.text = $"{barCounterVariable.Content}";
            factorText.text = $"X{factor}";
            sumText.text = $"{barCounterVariable.Content * factor}";

            // Debug.Log($"[END] set values: bar Counter: {barCounterVariable.Content}, " +
            //           $"bar counter text: {barCounterText.text}, " +
            //           $"factor: {factor}, " +
            //           $"factor text: {factorText.text}, " +
            //           $"sum: {barCounterVariable.Content * factor}, " +
            //           $"sum text: {sumText.text}");

        }

        private void FinishConfigArrived(EventSO ev)
        {
            var eve = ev as FinishConfigEvent;
            factor = eve.finishConfig.Factor;
            RenderView();
        }

        private void GameStateChanged(GameStateConfig newGameState)
        {
            gameObject.SetActive(newGameState.showEndGameViewsAndAnimations);
        }

        // connected to animation event
        public void EndSequenceFinished()
        {
            endSequenceFinishedEvent.Invoke();
        }

        // connected to animation
        public void PlayParticle()
        {
            playParticleEffectEvent.Invoke();
        }

        private void RegisterLifetimeListeners()
        {
            currentGameStateVariable.RegisterListener(GameStateChanged);
            finishConfigEvent.RegisterListener(FinishConfigArrived);
            barCounterVariable.RegisterListener(BarCounterChanged);
        }

        private void UnregisterLifetimeListeners()
        {
            currentGameStateVariable.UnregisterListener(GameStateChanged);
            finishConfigEvent.UnregisterListener(FinishConfigArrived);
            barCounterVariable.UnregisterListener(BarCounterChanged);
        }

        private void ValidateReferences()
        {
            Debug.Assert(barCounterVariable != null, "barCounterVariable != null");
            Debug.Assert(currentGameStateVariable != null, "currentGameStateVariable != null");
            Debug.Assert(playParticleEffectEvent != null, "playParticleEffectEvent != null");
            Debug.Assert(finishConfigEvent != null, "finishConfigEvent != null");
            Debug.Assert(endSequenceFinishedEvent != null, "endSequenceFinishedEvent != null");
            Debug.Assert(animator != null, "animator != null");
            Debug.Assert(barCounterText != null, "barCounterText != null");
            Debug.Assert(factorText != null, "factorText != null");
            Debug.Assert(sumText != null, "sumText != null");
        }
    }
}