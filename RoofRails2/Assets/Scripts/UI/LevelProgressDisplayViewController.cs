using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RoofRails2
{
    [Serializable]
    public class LevelProgressDisplayViewData
    {
        public IntVariable levelNumberVariable;
        public FloatVariable progressVariable;
    }
    public class LevelProgressDisplayViewController : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private GameStateConfigVariable currentGameStateVariable;

        [Header("View Components")]
        [SerializeField] private TextMeshProUGUI levelNumber;
        [SerializeField] private Image levelProgressImage;

        [Header("Dynamic Data")]
        [SerializeField] private LevelProgressDisplayViewData data;

        public LevelProgressDisplayViewData Data
        {
            get => data;
            set
            {
                data = value;
                UnregisterListeners();
                RegisterListeners();
                RenderView();
            }
        }

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void LevelNumberChanged(int newNumber)
        {
            RenderView();
        }

        private void LevelProgressChanged(float newProgress)
        {
            RenderView();
        }

        private void RenderView()
        {
            if (!currentGameStateVariable.Content.updateLevelProgress)
            {
                return;
            }

            var lvlNum = data.levelNumberVariable != null ? data.levelNumberVariable.Content : 0;
            var progress = data.progressVariable != null ? data.progressVariable.Content : 0f;
            levelNumber.text = $"{lvlNum}";
            levelProgressImage.fillAmount = progress;
        }

        private void RegisterListeners()
        {
            data.levelNumberVariable.RegisterListener(LevelNumberChanged);
            data.progressVariable.RegisterListener(LevelProgressChanged);
        }

        private void UnregisterListeners()
        {
            data.levelNumberVariable.UnregisterListener(LevelNumberChanged);
            data.progressVariable.UnregisterListener(LevelProgressChanged);
        }

        private void ValidateReferences()
        {
            Debug.Assert(currentGameStateVariable != null, "currentGameStateVariable != null");
            Debug.Assert(levelNumber != null, "levelNumber != null");
            Debug.Assert(levelProgressImage != null, "levelProgressImage != null");
        }
    }
}
