using System;
using UnityEngine;

namespace RoofRails2
{
    public class ShortenBarByTrigger : MonoBehaviour
    {
        [SerializeField] private FloatEvent shortenBarByAmountEvent;

        [SerializeField] private float shortenSpeed;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnTriggerStay(Collider other)
        {
            if (!other.CompareTag("Player"))
            {
                return;
            }

            var shortenAmount = shortenSpeed * Time.deltaTime;
            shortenBarByAmountEvent.Invoke(shortenAmount);

            Debug.Log($"[LAVA] shorten bar by: {shortenAmount}");
        }

        private void ValidateReferences()
        {
            Debug.Assert(shortenBarByAmountEvent != null, "shortenBarByAmountEvent != null");
        }
    }
}