using System;
using TMPro;
using UnityEngine;

namespace RoofRails2
{
    public class FinishPlatformController : MonoBehaviour
    {
        [Header("Config")]
        [SerializeField] private FinishConfig finishConfig;

        [Header("View Components")]
        [SerializeField] private TextMeshPro factorText;
        [SerializeField] private Renderer landingRenderer;
        [SerializeField] private Transform stretchTrans;

        public FinishConfig FinishConfig => finishConfig;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RenderView();
        }

        private void RenderView()
        {
            factorText.text = finishConfig.Text;

            var localPos = factorText.transform.localPosition;
            localPos.z = finishConfig.Len * 0.5f;
            factorText.transform.localPosition = localPos;

            landingRenderer.material = finishConfig.Material;

            var localScale = stretchTrans.localScale;
            localScale.z = finishConfig.Len;
            stretchTrans.localScale = localScale;
        }

        private void ValidateReferences()
        {
            if (factorText == null)
            {
                factorText = GetComponentInChildren<TextMeshPro>();
            }

            Debug.Assert(finishConfig != null, "finishConfig != null");
            Debug.Assert(factorText != null, "factorText != null");
            Debug.Assert(landingRenderer != null, "landingRenderer != null");
            Debug.Assert(stretchTrans != null, "stretchTrans != null");
        }
    }
}
