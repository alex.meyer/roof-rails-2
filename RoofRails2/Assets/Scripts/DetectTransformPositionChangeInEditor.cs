using System;
using UnityEngine;


[ExecuteInEditMode]
public class DetectTransformPositionChangeInEditor : MonoBehaviour
{
    private Vector3 m_oldPos;
    public Action<Vector3> OnTransformPositionChanged;

    private void OnEnable()
    {
        m_oldPos = transform.position;
    }

    private void Update()
    {
        var pos = transform.position;
        if (m_oldPos == pos)
        {
            return;
        }

        OnTransformPositionChanged?.Invoke(pos);
        m_oldPos = pos;
    }
}
