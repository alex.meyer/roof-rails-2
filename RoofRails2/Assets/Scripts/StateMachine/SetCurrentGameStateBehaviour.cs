using UnityEngine;

namespace RoofRails2
{
    public class SetCurrentGameStateBehaviour : StateMachineBehaviour
    {
        [SerializeField] private GameStateConfig config;
        [SerializeField] private GameStateConfigVariable currentGameStateVariable;

        protected void Awake()
        {
            ValidateReferences();
        }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            currentGameStateVariable.Content = config;
        }

        private void ValidateReferences()
        {
            Debug.Assert(config != null, "config != null");
            Debug.Assert(currentGameStateVariable != null, "currentGameState != null");
        }
    }
}