using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RoofRails2
{
    public class FinishPlatformsConstructionHelper : MonoBehaviour
    {
        [SerializeField] private GlobalConfig globalConfig;
        [SerializeField] private List<FinishPlatformController> allFinishPlatforms;

        private void OnValidate()
        {
            allFinishPlatforms = GetComponentsInChildren<FinishPlatformController>().ToList();

            var firstPlatform = allFinishPlatforms.FirstOrDefault(pl => pl.FinishConfig.OffsetPosZ == 0f);
            if (firstPlatform == null)
            {
                Debug.LogWarning($"[HELPER] Couldn't find first platform");
                return;
            }

            firstPlatform.transform.position = transform.position;
            foreach (var pl in allFinishPlatforms)
            {
                var pos = transform.position;
                pos.z += pl.FinishConfig.OffsetPosZ;
                pl.transform.position = pos;
            }
        }
    }
}
