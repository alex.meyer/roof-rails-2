using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewFloatVariable", menuName = "DataSO/FloatVariable", order = 0)]
    public class FloatVariable : BaseVariable<float> { }
}