using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewGameStateConfigVariable", menuName = "DataSO/GameStateConfigVariable", order = 0)]
    public class GameStateConfigVariable : BaseVariable<GameStateConfig>
    {

    }
}