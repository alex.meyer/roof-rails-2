using UnityEngine;
using System;
using UnityEngine.Serialization;

namespace RoofRails2
{
    public class BaseVariable<T> : ScriptableObject
    {
#if UNITY_EDITOR
        [Header("Explain me")]
        [TextArea] public string developerDescription;
#endif
        [Header("Static content")]
        [SerializeField] private T initial;

        [Header("DYNAMIC VALUE")]
        [SerializeField] private T current;

        public T Content
        {
            get => current;
            set
            {
                current = value;
                _valueChanged?.Invoke(current);
            }
        }

        private Action<T> _valueChanged;

        protected virtual void OnEnable()
        {
            current = initial;
        }

        public void Reset()
        {
            Content = initial;
        }

        public void RegisterListener(Action<T> listener)
        {
            _valueChanged += listener;
        }

        public void UnregisterListener(Action<T> listener)
        {
            _valueChanged -= listener;
        }
    }
}
