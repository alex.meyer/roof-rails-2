using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewVector3Variable", menuName = "DataSO/Vector3Variable")]
    public class Vector3Variable : BaseVariable<Vector3>
    {
        public float Magnitude => Content.magnitude;
        public Vector3 Direction => Content.normalized;
    }
}
