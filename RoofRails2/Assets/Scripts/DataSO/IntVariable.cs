using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewIntVariable", menuName = "DataSO/IntVariable")]
    public class IntVariable : BaseVariable<int> { }
}