using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ItchyOwl.ObjectPooling;
using UnityEngine;

namespace RoofRails2
{
    [Serializable]
    public class GrindParticle
    {
        public GameObject railGo;
        public ParticleSystem particle;
    }

    public class SelfForwardRunningLeftRightMovingCharacterControl : MonoBehaviour
    {
        private static readonly int hashGrounded = Animator.StringToHash("Grounded");
        private static readonly int hashMoveSpeed = Animator.StringToHash("MoveSpeed");
        private static readonly int hashLand = Animator.StringToHash("Land");
        private static readonly int hashJump = Animator.StringToHash("Jump");

        [Header("Variables")]
        [SerializeField] private CameraVariable mainCameraVariable;
        [SerializeField] private GameStateConfigVariable currentGameStateConfigVariable;

        [Header("Events")]
        [SerializeField] private EventSO detachBarEvent;
        [SerializeField] private EventSO barLostEvent;
        [SerializeField] private FinishConfigEvent reachedGoalEvent;
        [SerializeField] private EventSO firstMouseClickEvent;
        [SerializeField] private EventSO reloadLevelEvent;

        [Header("Meta Data")]
        [SerializeField] private float leftRightRange = 5.6f;
        [SerializeField] private float forwardSpeed = 2f;
        [SerializeField] private float lostImpulseStrength = 1f;
        [SerializeField] private float lostImpulseOffset = 1f;
        [SerializeField] private float normalizedPartialScreenWidthForInput = 0.6f;
        [SerializeField] private float onRailXCorrectionStrength = 1f;

        [Header("Prefabs")]
        [SerializeField] private GameObject grindParticlePrefab;

        [Header("Components")]
        [SerializeField] private Animator animator;
        [SerializeField] private Rigidbody rigidBody;
        [SerializeField] private BarController bar;

        [Header("Dynamic Data")]

        [SerializeField] private bool wasGrounded;
        [SerializeField] private bool isGrounded;
        [SerializeField] private int isGrindingAmount;
        [SerializeField] private Vector3 grindingDirection;

        [SerializeField] private bool inputDown = false;
        [SerializeField] private bool inputRelease = false;
        [SerializeField] private float initialInputScreenX;
        [SerializeField] private float initialInputWorldX;
        [SerializeField] private float currentInputScreenX;
        [SerializeField] private float onRailXCorrection = 0f;

        private readonly List<Collider> m_collisions = new List<Collider>();
        private readonly List<Collider> m_barCollisions = new List<Collider>();
        [SerializeField] private List<GrindParticle> particles;
        private static readonly int hashWave = Animator.StringToHash("Wave");

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

#region collision

        private void OnCollisionEnter(Collision collision)
        {
            ContactPoint[] contactPoints = collision.contacts;
            for (int i = 0; i < contactPoints.Length; i++)
            {
                if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0.5f)
                {
                    if (collision.collider.CompareTag("Rail"))
                    {
                        onRailXCorrection = 0f;
                        var myCollider = collision.GetContact(i).thisCollider;
                        // get info if contact with player or with bar
                        if (myCollider.transform.parent == bar.transform)
                        {
                            if (!m_barCollisions.Contains(collision.collider))
                            {
                                m_barCollisions.Add(collision.collider);
                            }

                            isGrindingAmount = m_barCollisions.Count;
                            if (isGrindingAmount > 0)
                            {
                                grindingDirection = m_barCollisions[0].transform.parent.forward;
                            }

                            // Debug.Log($"[COLL] bar collided with rail: {collision.collider.gameObject.name}, added it to rails colliders. is grinding: {isGrindingAmount}, grinding driection: {grindingDirection}, added to bar collisions list");

                        }
                        else if (myCollider.transform == transform)
                        {
                            // get relative pos and give the character a position correction out from rail
                            var diffX = collision.collider.transform.position.x - myCollider.transform.position.x;
                            onRailXCorrection = diffX <= 0 ? onRailXCorrectionStrength : -onRailXCorrectionStrength;
                            // don't add rail to colliders if player touches it

                            // Debug.Log(
                            //     $"[COLL] player collided with rail. set the correction x value: {onRailXCorrection}, not added to any collision lists");
                        }
                    }
                    else
                    {
                        if (!m_collisions.Contains(collision.collider))
                        {
                            m_collisions.Add(collision.collider);
                        }

                        isGrounded = m_collisions.Any(col => col.CompareTag("Floor"));

                        // Debug.Log(
                        //     $"[COLL] collision enter with {collision.collider.gameObject.name}, is grounded: {isGrounded}, added to collisions list");
                    }
                }
            }

            var finishPlatformController = collision.collider.GetComponentInParent<FinishPlatformController>();
            if (finishPlatformController != null)
            {
                ReachedFinishPlatform(finishPlatformController.FinishConfig);
            }

            if (!collision.collider.CompareTag("Rail"))
            {
                return;
            }

            var worldPos = collision.contacts[0].point;
            var go = grindParticlePrefab.Spawn();
            var trans = go.transform;
            trans.position = worldPos;
            trans.parent = bar.transform;
            trans.LookAt(trans.position - grindingDirection);

            var grindParticle = new GrindParticle
            {
                railGo = collision.collider.gameObject,
                particle = go.GetComponent<ParticleSystem>()
            };

            particles.Add(grindParticle);
        }

        private void OnCollisionStay(Collision collision)
        {
            if (collision.collider.CompareTag("Floor"))
            {
                var contactPoints = collision.contacts;
                var validSurfaceNormal = false;
                foreach (var t in contactPoints)
                {
                    if (!(Vector3.Dot(t.normal, Vector3.up) > 0.5f))
                    {
                        continue;
                    }

                    validSurfaceNormal = true;
                    break;
                }

                if (validSurfaceNormal)
                {
                    if (!m_collisions.Contains(collision.collider))
                    {
                        m_collisions.Add(collision.collider);
                    }
                }
                else
                {
                    if (m_collisions.Contains(collision.collider))
                    {
                        m_collisions.Remove(collision.collider);
                    }
                }
            }

            isGrounded = m_collisions.Any(col => col.CompareTag("Floor"));
            isGrindingAmount = m_barCollisions.Count;

            // Debug.Log(
            //     $"[COLL] collision stay with {collision.collider.gameObject.name}, is grounded: {isGrounded}, is grinding: {isGrindingAmount}, direction: {grindingDirection}");
        }

        private void OnCollisionExit(Collision collision)
        {
            if (m_collisions.Contains(collision.collider))
            {
                m_collisions.Remove(collision.collider);
            }

            if (m_barCollisions.Contains(collision.collider))
            {
                m_barCollisions.Remove(collision.collider);
            }

            isGrounded = m_collisions.Any(col => col.CompareTag("Floor"));
            isGrindingAmount = m_barCollisions.Count;

            // Debug.Log($"[COLL] collision exit with {collision.collider.gameObject.name}, is grounded: {isGrounded}, is grinding: {isGrindingAmount}, direction: {grindingDirection}");

            var grindParticle = particles.FirstOrDefault(p => p.railGo == collision.collider.gameObject);
            if (grindParticle == null)
            {
                return;
            }

            grindParticle.particle.gameObject.Recycle();
            particles.Remove(grindParticle);
        }

        #endregion

        private void FixedUpdate()
        {
            animator.SetBool(hashGrounded, isGrounded);

            if (currentGameStateConfigVariable.Content.waitForFirstTap ||
                currentGameStateConfigVariable.Content.waitForContinue)
            {
                if (currentGameStateConfigVariable.Content.waitForContinue &&
                    inputDown &&
                    !inputRelease)
                {
                    WaitForRelease();
                    return;
                }

                WaitForTap();
                return;
            }

            if (currentGameStateConfigVariable.Content.characterIsWaving)
            {
                AutoMoveForwardSetPosition(0f);
                return;
            }

            if (currentGameStateConfigVariable.Content.characterMovementPossible)
            {
                Movement();
            }

            // Debug.Log($"[CHAR] velocity: {rigidBody.velocity}");

            wasGrounded = isGrounded;
        }

        private void ReachedFinishPlatform(FinishConfig finishConfig)
        {
            reachedGoalEvent.finishConfig = finishConfig;
            reachedGoalEvent.Invoke();
        }

        private void DetachBar(EventSO ev)
        {
            DetachBar();
        }

        private void DetachBar()
        {
            if (!bar.TryGetComponent<Rigidbody>(out var rb))
            {
                rb = bar.gameObject.AddComponent<Rigidbody>();
            }

            var barTrans = bar.transform;
            barTrans.parent = null;
            rb.velocity = rb.velocity;
            var lostImpulseOffsetPos =
                barTrans.position + Vector3.right * lostImpulseOffset * barTrans.localScale.y * 0.5f;
            rb.AddForceAtPosition(Vector3.up * lostImpulseStrength, lostImpulseOffsetPos, ForceMode.Impulse);

            barLostEvent.Invoke();
        }

        private void WaitForRelease()
        {
            inputRelease = false;
            if (Application.platform != RuntimePlatform.Android &&
                Application.platform != RuntimePlatform.IPhonePlayer)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    inputRelease = true;
                }
            }
            else
            {
                var touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Ended)
                {
                    inputRelease = true;
                }
            }
        }

        private void WaitForTap()
        {
            var pos = transform.position;

            if (Application.platform != RuntimePlatform.Android &&
                Application.platform != RuntimePlatform.IPhonePlayer)
            {
                var mousePos = Input.mousePosition;

                if (Input.GetMouseButtonDown(0))
                {
                    inputDown = true;
                    initialInputScreenX = Input.mousePosition.x;
                    initialInputWorldX = pos.x;
                }

                if (Input.GetMouseButtonUp(0))
                {
                    inputDown = false;
                }

                if (inputDown)
                {
                    currentInputScreenX = Input.mousePosition.x;
                }
            }
            else
            {
                var touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    inputDown = true;
                    initialInputScreenX = touch.position.x;
                    initialInputWorldX = pos.x;
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    inputDown = false;
                }

                if (inputDown)
                {
                    currentInputScreenX = touch.position.x;
                }
            }

            if (inputDown)
            {
                if (currentGameStateConfigVariable.Content.waitForFirstTap)
                {
                    firstMouseClickEvent.Invoke();
                }

                if (currentGameStateConfigVariable.Content.waitForContinue)
                {
                    reloadLevelEvent.Invoke();
                }
            }
        }

        private void GetInputAndSetXPosition()
        {
            var pos = transform.position;

            if (Application.platform != RuntimePlatform.Android &&
                Application.platform != RuntimePlatform.IPhonePlayer)
            {
                var mousePos = Input.mousePosition;

                if (Input.GetMouseButtonDown(0))
                {
                    inputDown = true;
                    initialInputScreenX = Input.mousePosition.x;
                    initialInputWorldX = pos.x;
                }

                if (Input.GetMouseButtonUp(0))
                {
                    inputDown = false;
                }

                if (inputDown)
                {
                    currentInputScreenX = Input.mousePosition.x;
                }
            }
            else if (Input.touches.Length > 0)
            {
                var touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    inputDown = true;
                    initialInputScreenX = touch.position.x;
                    initialInputWorldX = pos.x;
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    inputDown = false;
                }

                if (inputDown)
                {
                    currentInputScreenX = touch.position.x;
                }
            }

            if (inputDown)
            {
                initialInputWorldX += onRailXCorrection;
                var screenDiff = currentInputScreenX - initialInputScreenX;
                var maxScreenInputRange = Screen.width * normalizedPartialScreenWidthForInput;
                var worldDiff = screenDiff * leftRightRange / maxScreenInputRange;
                pos.x = initialInputWorldX + worldDiff;
            }
            else
            {
                pos.x += onRailXCorrection;
            }

            var halfRange = leftRightRange * 0.5f;
            pos.x = Mathf.Clamp(pos.x, -halfRange, halfRange);
            transform.position = pos;

            // Debug.Log($"[INPUT] current pos: {currentInputScreenX}," +
            //           $" initial screen x: {initialInputScreenX}," +
            //           $" initial world X: {initialInputWorldX}," +
            //           $" current pos x: {pos.x}");
        }

        private void AutoMoveForwardSetPosition(float v)
        {
            var trans = transform;
            var pos = trans.position;

            pos += (isGrindingAmount > 1 ? grindingDirection : trans.forward) * v * forwardSpeed * Time.deltaTime;
            trans.position = pos;

            animator.SetFloat(hashMoveSpeed, v);

            // Debug.Log($"[INPUT] auto move forward: grinding direction: {grindingDirection}, pos: {pos}");
        }

        private void Movement()
        {
            // grinding fail state
            if (isGrindingAmount == 1)
            {
                DetachBar();

                Debug.Log($"[CHAR] detach bar");
            }

            GetInputAndSetXPosition();
            AutoMoveForwardSetPosition(1f);

            JumpingAndLanding();
        }

        private IEnumerator WaveFrequently()
        {
            while (true)
            {
                animator.SetTrigger(hashWave);
                yield return new WaitForSeconds(1f);
            }
        }

        private void JumpingAndLanding()
        {
            if (!wasGrounded && isGrounded)
            {
                animator.SetTrigger(hashLand);
            }

            if (!isGrounded && wasGrounded)
            {
                animator.SetTrigger(hashJump);
            }
        }

        private void GameStateChanged(GameStateConfig newGameState)
        {
            // Here the one time actions directly if game state changes
            if (newGameState.characterIsWaving)
            {
                StartCoroutine(WaveFrequently());
            }
            else
            {
                // for now there is only one...
                StopAllCoroutines();
            }
        }

        private void RegisterListeners()
        {
            detachBarEvent.RegisterListener(DetachBar);
            currentGameStateConfigVariable.RegisterListener(GameStateChanged);
        }

        private void UnregisterListeners()
        {
            detachBarEvent.UnregisterListener(DetachBar);
            currentGameStateConfigVariable.UnregisterListener(GameStateChanged);
        }

        private void ValidateReferences()
        {
            animator = GetComponent<Animator>();
            rigidBody = GetComponent<Rigidbody>();
            bar = GetComponentInChildren<BarController>();

            Debug.Assert(reachedGoalEvent != null, "reachedGoalEvent != null");
            Debug.Assert(currentGameStateConfigVariable != null, "currentGameStateConfigVariable != null");
            Debug.Assert(mainCameraVariable != null, "mainCameraVariable != null");
            Debug.Assert(detachBarEvent != null, "fallenThroughBarsEvent != null");
            Debug.Assert(barLostEvent != null, "barLostEvent != null");
            Debug.Assert(firstMouseClickEvent != null, "firstMouseClickEvent != null");
            Debug.Assert(reloadLevelEvent != null, "reloadLevelEvent != null");
            Debug.Assert(animator != null, "animator != null");
            Debug.Assert(rigidBody != null, "rigidBody != null");
            Debug.Assert(bar != null, "bar != null");
            Debug.Assert(grindParticlePrefab != null, "grindParticlePrefab != null");
        }
    }
}