using UnityEngine;

namespace RoofRails2
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ActivateParticleEffectByEvent : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private EventSO activateParticleEvent;

        [Header("View Components")]
        [SerializeField] private ParticleSystem particles;

        [Header("Meta Data")]
        [SerializeField] private string id;

        private void Awake()
        {
            ValidateReferences();
            RegisterLifetimeListeners();
        }

        private void OnDestroy()
        {
            UnregisterLifetimeListeners();
        }

        private void PlayParticle(EventSO ev)
        {
            gameObject.SetActive(true); // if deactivated
            particles.Play();
        }

        private void RegisterLifetimeListeners()
        {
            activateParticleEvent.RegisterListener(PlayParticle);
        }

        private void UnregisterLifetimeListeners()
        {
            activateParticleEvent.UnregisterListener(PlayParticle);
        }

        private void ValidateReferences()
        {
            particles = GetComponent<ParticleSystem>();

            Debug.Assert(activateParticleEvent != null, "activateParticleEvent != null");
            Debug.Assert(particles != null, "particles != null");
        }
    }
}