using System;
using UnityEngine;

namespace RoofRails2
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ActivateParticleEffectByGameState : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private GameStateConfigVariable currentGameStateVariable;

        [Header("View Components")]
        [SerializeField] private ParticleSystem particles;

        [Header("Meta Data")]
        [SerializeField] private string id;

        private void Awake()
        {
            ValidateReferences();
            RegisterLifetimeListeners();
        }

        private void OnDestroy()
        {
            UnregisterLifetimeListeners();
        }

        private void OnEnable()
        {
            particles.Play();
            GameStateChanged(currentGameStateVariable.Content);
        }

        private void OnDisable()
        {
            particles.Stop();
        }

        private void GameStateChanged(GameStateConfig newGameState)
        {
            gameObject.SetActive(newGameState.activateParticleEffectId == id);
        }

        private void RegisterLifetimeListeners()
        {
            currentGameStateVariable.RegisterListener(GameStateChanged);
        }

        private void UnregisterLifetimeListeners()
        {
            currentGameStateVariable.UnregisterListener(GameStateChanged);
        }

        private void ValidateReferences()
        {
            particles = GetComponent<ParticleSystem>();

            Debug.Assert(currentGameStateVariable != null, "currentGameStateVariable != null");
            Debug.Assert(particles != null, "particles != null");
        }
   }
}