using System;
using UnityEngine;

namespace RoofRails2
{
    public class BarController : MonoBehaviour
    {
        [Header("Events")]
        [SerializeField] private ItemConfigEvent collectItemEvent;
        [SerializeField] private FloatEvent shortenBarByAmountEvent;

        [Header("Meta Data")]
        [SerializeField] private float minBarLen = 1f;

        [Header("Dynamic Meta Data")]
        [SerializeField] private Vector3 originalLocalScale;

        private Transform m_thisTrans;

        private void Awake()
        {
            ValidateReferences();
            originalLocalScale = transform.localScale;
        }

        private void OnEnable()
        {
            RegisterListeners();
            transform.localScale = originalLocalScale;
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void CollectItem(EventSO ev)
        {
            if (collectItemEvent.config.GrowingAmount <= 0f)
            {
                return;
            }

            var localScale = m_thisTrans.localScale;
            localScale.y += collectItemEvent.config.GrowingAmount;
            m_thisTrans.localScale = localScale;
        }

        private void ShortenBarByAmount(EventSO ev)
        {
            var localScale = m_thisTrans.localScale;
            localScale.y = Mathf.Max(localScale.y - shortenBarByAmountEvent.value, originalLocalScale.y);

            Debug.Log($"[BAR] shorten by amount: {shortenBarByAmountEvent.value}, old scale: {m_thisTrans.localScale.y}, new scale: {localScale.y}");

            m_thisTrans.localScale = localScale;
        }

        private void RegisterListeners()
        {
            collectItemEvent.RegisterListener(CollectItem);
            shortenBarByAmountEvent.RegisterListener(ShortenBarByAmount);
        }

        private void UnregisterListeners()
        {
            collectItemEvent.UnregisterListener(CollectItem);
            shortenBarByAmountEvent.UnregisterListener(ShortenBarByAmount);
        }

        private void ValidateReferences()
        {
            m_thisTrans = transform;

            Debug.Assert(collectItemEvent != null, "collectItemEvent != null");
            Debug.Assert(shortenBarByAmountEvent != null, "shortenBarByAmountEvent != null");
        }
    }
}