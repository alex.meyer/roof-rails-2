using ItchyOwl.ObjectPooling;
using UnityEngine;

namespace RoofRails2
{
    public class CollectItem : MonoBehaviour
    {
        [SerializeField] private ItemConfig config;
        [SerializeField] private ItemConfigEvent collectItemEvent;
        [SerializeField] private GameObject textPrefab;

        [SerializeField] private Vector3 offsetForText = new Vector3(0.6f, -1.7f, 0f);

        private void OnTriggerEnter(Collider other)
        {
            if (string.IsNullOrEmpty(config.CollectorTagName) || !other.CompareTag(config.CollectorTagName))
            {
                return;
            }

            Debug.Log($"[ITEM] Collect Item trigger enter");
            collectItemEvent.Invoke(config);
            if (!config.IsDisappearing)
            {
                return;
            }

            if (collectItemEvent == null)
            {
                return;
            }

            // Todo: ParticleSystem should self recycle
            var transPos = transform.position;
            var go = config.DisappearParticlesPrefab.Spawn();
            go.transform.position = transPos;
            if (textPrefab != null)
            {
                var textGo = textPrefab.Spawn();
                textGo.transform.position = transPos + offsetForText;
                var ctrl = textGo.GetComponent<FadeAwayTextViewController>();
                ctrl.Data = new FadeAwayTextViewData { text = $"+{collectItemEvent.config.VisualGrowingAmount}" };
            }

            // These objects are pre instantiated -> no pool
            Destroy(gameObject);
        }

        private void Awake()
        {
            ValidateReferences();
        }

        private void ValidateReferences()
        {
            Debug.Assert(config != null, "config != null");
            Debug.Assert(collectItemEvent != null, "collectItemEvent != null");
        }

    }
}