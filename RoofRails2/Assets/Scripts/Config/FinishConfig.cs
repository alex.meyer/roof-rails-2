using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewFinishConfig", menuName = "Config/FinishConfig", order = 0)]
    public class FinishConfig : ScriptableObject
    {
        [SerializeField] private Material material;
        [SerializeField] private string textPreFix;
        [SerializeField] private int factor;
        [SerializeField] private float len;
        [SerializeField] private float offsetPosZ;

        public Material Material => material;
        public string Text => $"{textPreFix}{factor}";
        public int Factor => factor;
        public float Len => len;
        public float OffsetPosZ
        {
            get => offsetPosZ;
            set => offsetPosZ = value;
        }
    }
}