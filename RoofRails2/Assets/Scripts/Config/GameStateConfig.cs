using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewGameStateConfig", menuName = "Config/GameStateConfig", order = 0)]
    public class GameStateConfig : ScriptableObject
    {
        public bool characterMovementPossible;
        public bool characterIsWaving;
        public bool startWonTimer;
        public bool startLostTimer;
        public bool showStartSequenceViews;
        public bool showInGameHud;
        public bool showEndGameViewsAndAnimations;
        public bool waitForFirstTap;
        public string activateParticleEffectId;
        public bool loosingBarCausesLost;
        public bool detachCamera;
        public bool showTapToContinue;
        public bool waitForContinue;
        public bool updateLevelProgress;
    }
}