using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewItemConfig", menuName = "Config/ItemConfig", order = 0)]
    public class ItemConfig : ScriptableObject
    {
        [Header("Bar Extension")]
        [SerializeField] private float growingAmount = 0f;
        [SerializeField] private int visualGrowingAmount;

        [Header("Diamond")]
        [SerializeField] private int diamondAmount;

        [Header("Common")]
        [SerializeField] private bool isDisappearing = true;
        [SerializeField] private GameObject disappearParticlesPrefab;
        [SerializeField] private string collectorTagName;

        public float GrowingAmount => growingAmount;
        public int VisualGrowingAmount => visualGrowingAmount;
        public int DiamondAmount => diamondAmount;
        public bool IsDisappearing => isDisappearing;
        public GameObject DisappearParticlesPrefab => disappearParticlesPrefab;
        public string CollectorTagName => collectorTagName;
    }
}
