using System;
using System.Collections.Generic;
using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewGlobalConfig", menuName = "Config/GlobalConfig")]
    public class GlobalConfig : ScriptableObject
    {
        [SerializeField] private List<ItemConfig> itemConfigs;
        [SerializeField] private List<FinishConfig> finishConfigs;

        public IReadOnlyList<ItemConfig> ItemConfigs => itemConfigs;
        public IReadOnlyList<FinishConfig> FinishConfigs => finishConfigs;

        private void OnValidate()
        {
            var offset = 0f;
            foreach (var finishConfig in finishConfigs)
            {
                finishConfig.OffsetPosZ = offset;
                offset += finishConfig.Len;
            }
        }
    }
}