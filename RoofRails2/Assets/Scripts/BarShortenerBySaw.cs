using System.Collections.Generic;
using ItchyOwl.ObjectPooling;
using UnityEngine;

namespace RoofRails2
{
    public class BarShortenerBySaw : MonoBehaviour
    {
        [Header("Prefabs")]
        [SerializeField] private GameObject barFragment;

        [Header("Components")]
        [SerializeField] private BarController bar;

        [Header("Meta Data")]
        [SerializeField] private float impulseStrength = 2f;
        [SerializeField] private float normalizedOffset = 0.1f;

        [SerializeField] private List<Collider> sawColliders;
        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
            sawColliders.Clear();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void OnTriggerEnter(Collider other)
        {
            // Debug.Log($"[BAR] trigger enter with {other.gameObject.name}");

            if (!other.CompareTag("Saw") || sawColliders.Contains(other))
            {
                return;
            }

            sawColliders.Add(other);

            var barTransform = bar.transform;
            var barPosition = barTransform.position;
            var otherPosition = other.transform.position;
            var diffFromCenter = otherPosition.x - barPosition.x;
            var barLocalScale = barTransform.localScale;
            var barLength = barLocalScale.y;
            var leftSide = diffFromCenter < 0;
            var toCut = barLength * 0.5f - Mathf.Abs(diffFromCenter);

            Debug.Log($"[BAR] other x: {otherPosition.x}, bar x: {barPosition.x}, bar length: {barLength}, diffFromCenter {diffFromCenter}, to cut: {toCut}, side: {(leftSide ? "left" : "right")}");

            // Todo: create new bar end to throw away

            var go = barFragment.Spawn();
            var goTrans = go.transform;
            var sign = (leftSide ? -1f : 1f);
            var halfFragmentLengthWithSign = toCut * 0.5f * sign;
            goTrans.position = barPosition + Vector3.right * (diffFromCenter + halfFragmentLengthWithSign);
            goTrans.rotation = barTransform.rotation;
            var localFragmentScale = goTrans.localScale;
            localFragmentScale.y = toCut;
            goTrans.localScale = localFragmentScale;
            var rb = go.GetComponent<Rigidbody>();
            var impulseOffset = Vector3.right * -halfFragmentLengthWithSign * normalizedOffset;
            rb.AddForceAtPosition(Vector3.up * impulseStrength, goTrans.position - impulseOffset,  ForceMode.Impulse);

            // Todo: shorten and reposition the bar to visualize the cut
            barLocalScale.y -= toCut;
            barTransform.localScale = barLocalScale;

            Debug.Log($"[BAR] bar length: {barLength}, diffFromCenter {diffFromCenter}, to cut: {toCut}, side: {(leftSide ? "left" : "right")}, new length: {barLocalScale.y}");
        }

        private void OnTriggerExit(Collider other)
        {
            Debug.Log($"[BAR] trigger exit from {other.gameObject.name}");
        }

        private void RegisterListeners() { }

        private void UnregisterListeners() { }

        private void ValidateReferences()
        {
            bar = GetComponentInChildren<BarController>();

            Debug.Assert(bar != null, "bar != null");
            Debug.Assert(barFragment != null, "barFragment != null");
        }
    }
}