using ItchyOwl.ObjectPooling;
using UnityEngine;

namespace RoofRails2
{
    public class SelfRecycleByAnimationEvent : MonoBehaviour
    {
        // connect with AnimationEvent
        public void Recycle()
        {
            gameObject.Recycle();
        }
    }
}