using System;
using System.Collections.Generic;
using UnityEngine;

namespace RoofRails2
{
    public class BarDistanceSlider : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private List<Transform> rails;

        [Header("Meta Data")]
        [SerializeField] private float distance;

        private void Start()
        {
            SetBars();
        }

        private void SetBars()
        {
            var amount = rails.Count;
            var odd = amount % 2 > 0;

            for (var i = 0; i < amount / 2; i++)
            {
                var rail1 = rails[i];
                var rail2 = rails[amount - 1 - i];
                var x = distance * (i + 0.5f);
                if (odd)
                {
                    x = distance * i;
                }
                SetX(rail1, x);
                SetX(rail2, -x);
            }

            if (!odd)
            {
                return;
            }

            var rail = rails[amount / 2];
            SetX(rail, 0);
        }

        private void SetX(Transform trans, float x)
        {
            var pos = trans.localPosition;
            pos.x = x;
            trans.localPosition = pos;
        }

        private void OnValidate()
        {
            SetBars();
        }
    }
}
