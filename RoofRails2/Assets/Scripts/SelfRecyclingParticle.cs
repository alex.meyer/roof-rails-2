using System;
using ItchyOwl.ObjectPooling;
using UnityEngine;

namespace RoofRails2
{
    [RequireComponent(typeof(ParticleSystem))]
    public class SelfRecyclingParticle : MonoBehaviour
    {
        [SerializeField] private ParticleSystem particleSystem;

        private void Awake()
        {
            ValidateReferences();
        }

        private void Update()
        {
            if (particleSystem.isPlaying)
            {
                return;
            }

            gameObject.Recycle();
        }

        private void ValidateReferences()
        {
            particleSystem = GetComponent<ParticleSystem>();

            Debug.Assert(particleSystem != null, "particleSystem != null");
        }

    }
}