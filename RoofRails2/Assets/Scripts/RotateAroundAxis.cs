using System;
using UnityEngine;

namespace RoofRails2
{
    public class RotateAroundAxis : MonoBehaviour
    {
        [SerializeField] private Vector3 localAxis = Vector3.up;
        [SerializeField] private float angularSpeed = 1f;

        private void Update()
        {
            var rotation = transform.localRotation;
            rotation *= Quaternion.Euler(localAxis * angularSpeed * Time.deltaTime);
            transform.localRotation = rotation;
        }
    }
}