using System.Collections.Generic;
using UnityEngine;

namespace RoofRails2
{
    public class SetMaterialTilingByYScaleValue : MonoBehaviour
    {
        [SerializeField] private List<Renderer> renderers;

        private void Awake()
        {
            ValidateReferences();
        }

        private void ValidateReferences()
        {
            foreach (var rend in renderers)
            {
                var mat = rend.material = new Material(rend.material);
                var tilingY = (int)transform.localScale.z;
                mat.mainTextureScale = new Vector2(1, tilingY);
            }
        }
    }
}