using System;
using UnityEngine;

namespace RoofRails2
{
    public class ThrowEventByTrigger : MonoBehaviour
    {
        [SerializeField] private EventSO triggeredEvent;
        private void OnTriggerEnter(Collider other)
        {
            triggeredEvent.Invoke();
        }
    }
}