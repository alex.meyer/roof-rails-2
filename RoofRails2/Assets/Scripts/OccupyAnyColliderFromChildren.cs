using System;
using UnityEngine;

namespace RoofRails2
{
    public class OccupyAnyColliderFromChildren : MonoBehaviour
    {
        private void Start()
        {
            var allCollidersInChildren = GetComponentsInChildren<Collider>();
            foreach (var coll in allCollidersInChildren)
            {
                coll.CopyComponent(gameObject);
                Destroy(coll);
            }
        }
    }
}