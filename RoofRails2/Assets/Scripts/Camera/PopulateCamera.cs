using System;
using UnityEngine;

namespace RoofRails2
{
    [RequireComponent(typeof(Camera))]
    public class PopulateCamera : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private CameraVariable cameraVariable;

        [Header("Components")]
        [SerializeField] private Camera cameraToPopulate;
        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void Start()
        {
            cameraVariable.Content = cameraToPopulate;
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void RegisterListeners() { }

        private void UnregisterListeners() { }

        private void ValidateReferences()
        {
            cameraToPopulate = GetComponent<Camera>();

            Debug.Assert(cameraVariable != null, "cameraVariable != null");
            Debug.Assert(cameraToPopulate != null, "cameraToPopulate != null");
        }
    }
}