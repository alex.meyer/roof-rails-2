using Cinemachine;
using UnityEngine;

namespace RoofRails2
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class DetachTargetFromVCamByGameState : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private GameStateConfigVariable currentGameStateVariable;

        [Header("View Components")]
        [SerializeField] private CinemachineVirtualCamera vCam;

        private void Awake()
        {
            ValidateReferences();
            RegisterLifetimeListeners();
        }

        private void OnEnable()
        {
            GameStateChanged(currentGameStateVariable.Content);
        }

        private void OnDestroy()
        {
            UnregisterLifetimeListeners();
        }

        private void GameStateChanged(GameStateConfig newGameState)
        {
            vCam.gameObject.SetActive(!newGameState.detachCamera);
        }

        private void RegisterLifetimeListeners()
        {
            currentGameStateVariable.RegisterListener(GameStateChanged);
        }

        private void UnregisterLifetimeListeners()
        {
            currentGameStateVariable.UnregisterListener(GameStateChanged);
        }

        private void ValidateReferences()
        {
            vCam = GetComponent<CinemachineVirtualCamera>();

            Debug.Assert(vCam != null, "vCam != null");
            Debug.Assert(currentGameStateVariable != null, "currentGameStateVariable != null");
        }
    }
}