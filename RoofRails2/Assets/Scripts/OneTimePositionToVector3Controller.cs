using System;
using UnityEngine;

namespace RoofRails2
{
    public class OneTimePositionToVector3Controller : MonoBehaviour
    {
        [SerializeField] private Vector3Variable targetVariable;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            targetVariable.Content = transform.position;
        }

        private void ValidateReferences()
        {
            Debug.Assert(targetVariable != null, "targetVariable != null");
        }
    }
}