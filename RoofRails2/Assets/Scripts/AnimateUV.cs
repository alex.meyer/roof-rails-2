using System;
using UnityEngine;

namespace RoofRails2
{
    public class AnimateUV : MonoBehaviour
    {
        [SerializeField] private Renderer rend;

        [SerializeField] private Vector2 uvSpeed = new Vector2(1f, 1f);

        private void Awake()
        {
            ValidateReferences();
        }

        private void Update()
        {
            var vec = rend.material.mainTextureOffset;
            vec += uvSpeed * Time.deltaTime;
            rend.material.mainTextureOffset = vec;
        }

        private void ValidateReferences()
        {
            rend = GetComponent<Renderer>();

            Debug.Assert(rend != null, "rend != null");
        }

    }
}