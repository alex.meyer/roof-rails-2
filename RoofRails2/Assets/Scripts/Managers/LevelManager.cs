using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RoofRails2
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private EventSO reloadCurrentLevelEvent;
        [SerializeField] private EventSO loadNextLevelEvent;

        [SerializeField] private List<string> allLevelNames;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void ReloadCurrentLevel(EventSO ev)
        {
            var currentMainSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentMainSceneName, LoadSceneMode.Single);
        }

        private void LoadNextLevel(EventSO ev)
        {
            var currentSceneIndex = allLevelNames.IndexOf(SceneManager.GetActiveScene().name);
            var nextSceneIndex = Mathf.Clamp(currentSceneIndex + 1, 0, allLevelNames.Count - 1);
            SceneManager.LoadScene(allLevelNames[nextSceneIndex]);
        }

        private void RegisterListeners()
        {
            reloadCurrentLevelEvent.RegisterListener(ReloadCurrentLevel);
            loadNextLevelEvent.RegisterListener(LoadNextLevel);
        }

        private void UnregisterListeners()
        {
            reloadCurrentLevelEvent.UnregisterListener(ReloadCurrentLevel);
            loadNextLevelEvent.UnregisterListener(LoadNextLevel);
        }

        private void ValidateReferences()
        {
            Debug.Assert(reloadCurrentLevelEvent != null, "reloadCurrentLevelEvent != null");
            Debug.Assert(loadNextLevelEvent != null, "loadNextLevelEvent != null");
        }
    }
}