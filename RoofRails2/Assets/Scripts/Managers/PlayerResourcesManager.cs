using UnityEngine;

namespace RoofRails2
{
    public class PlayerResourcesManager : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private IntVariable diamondsAmountVariable;
        [SerializeField] private IntVariable barCounterVariable;

        [Header("Events")]
        [SerializeField] private ItemConfigEvent itemCollectedEvent;
        [SerializeField] private FinishConfigEvent finishConfigEvent;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void ItemCollected(EventSO ev)
        {
            if (itemCollectedEvent.config == null)
            {
                return;
            }

            diamondsAmountVariable.Content += itemCollectedEvent.config.DiamondAmount;
            barCounterVariable.Content += itemCollectedEvent.config.VisualGrowingAmount;
        }

        private void FinishConfigArrived(EventSO ev)
        {
            var addDiamonds = barCounterVariable.Content * finishConfigEvent.finishConfig.Factor;
            diamondsAmountVariable.Content += addDiamonds;
        }

        private void RegisterListeners()
        {
            itemCollectedEvent.RegisterListener(ItemCollected);
            finishConfigEvent.RegisterListener(FinishConfigArrived);
        }

        private void UnregisterListeners()
        {
            itemCollectedEvent.UnregisterListener(ItemCollected);
            finishConfigEvent.UnregisterListener(FinishConfigArrived);
        }

        private void ValidateReferences()
        {
            Debug.Assert(diamondsAmountVariable != null, "diamondsAmountVariable != null");
            Debug.Assert(barCounterVariable != null, "barCounterVariable != null");
            Debug.Assert(itemCollectedEvent != null, "itemCollectedEvent != null");
            Debug.Assert(finishConfigEvent != null, "finishConfigEvent != null");
        }
    }
}