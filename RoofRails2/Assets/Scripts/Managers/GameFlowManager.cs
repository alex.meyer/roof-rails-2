using System.Collections;
using UnityEngine;

namespace RoofRails2
{
    public class GameFlowManager : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private Vector3Variable levelStartVector3Variable;
        [SerializeField] private Vector3Variable levelEndVector3Variable;
        [SerializeField] private Vector3Variable currentPlayerPositionVariable;
        [SerializeField] private FloatVariable levelProgressVariable;

        [Header("Events")]
        [SerializeField] private FinishConfigEvent reachedGoalEvent;
        [SerializeField] private EventSO firstMouseClickEvent;
        [SerializeField] private EventSO barLostEvent;
        [SerializeField] private EventSO reachedPreGoalEvent;

        [Header("State Machine")]
        [SerializeField] private Animator stateMachine;

        [Header("Meta Data")]
        [SerializeField] private float amountOfSecondsToWaitIfWon = 5f;

        private static readonly int hashReachedGoal = Animator.StringToHash("ReachedGoal");
        private static readonly int hashPlayerTappedScreen = Animator.StringToHash("PlayerTappedScreen");
        private static readonly int hashEndTimerEnds = Animator.StringToHash("EndTimerEnds");
        private static readonly int hashLostBar = Animator.StringToHash("LostBar");
        private static readonly int hashPastPreGoal = Animator.StringToHash("PastPreGoal");

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void OneOfThePositionsChanged(Vector3 v)
        {
            levelProgressVariable.Content =
                (currentPlayerPositionVariable.Content - levelStartVector3Variable.Content).magnitude /
                (levelEndVector3Variable.Content - levelStartVector3Variable.Content).magnitude;
        }

        private void ReachedGoal(EventSO ev)
        {
            stateMachine.SetTrigger(hashReachedGoal);
            StartCoroutine(WaitForSecondsAndSwitchToEndSequence(amountOfSecondsToWaitIfWon));
        }

        private IEnumerator WaitForSecondsAndSwitchToEndSequence(float seconds)
        {
            yield return new WaitForSeconds(seconds);

            stateMachine.SetTrigger(hashEndTimerEnds);
        }

        private void FirstMouseClick(EventSO ev)
        {
            stateMachine.SetTrigger(hashPlayerTappedScreen);
        }

        private void LostBar(EventSO ev)
        {
            stateMachine.SetTrigger(hashLostBar);
        }

        private void ReachedPreGoal(EventSO ev)
        {
            stateMachine.SetTrigger(hashPastPreGoal);
        }

        private void RegisterListeners()
        {
            currentPlayerPositionVariable.RegisterListener(OneOfThePositionsChanged);
            levelStartVector3Variable.RegisterListener(OneOfThePositionsChanged);
            levelEndVector3Variable.RegisterListener(OneOfThePositionsChanged);
            reachedGoalEvent.RegisterListener(ReachedGoal);
            firstMouseClickEvent.RegisterListener(FirstMouseClick);
            barLostEvent.RegisterListener(LostBar);
            reachedPreGoalEvent.RegisterListener(ReachedPreGoal);
        }

        private void UnregisterListeners()
        {
            currentPlayerPositionVariable.UnregisterListener(OneOfThePositionsChanged);
            levelStartVector3Variable.UnregisterListener(OneOfThePositionsChanged);
            levelEndVector3Variable.UnregisterListener(OneOfThePositionsChanged);
            reachedGoalEvent.UnregisterListener(ReachedGoal);
            firstMouseClickEvent.UnregisterListener(FirstMouseClick);
            barLostEvent.UnregisterListener(LostBar);
            reachedPreGoalEvent.UnregisterListener(ReachedPreGoal);
        }

        private void ValidateReferences()
        {
            Debug.Assert(levelStartVector3Variable != null, "levelStartVector3Variable != null");
            Debug.Assert(levelEndVector3Variable != null, "levelEndVector3Variable != null");
            Debug.Assert(currentPlayerPositionVariable != null, "currentPlayerPositionVariable != null");
            Debug.Assert(levelProgressVariable != null, "levelProgressVariable != null");
            Debug.Assert(reachedGoalEvent != null, "reachedGoalEvent != null");
            Debug.Assert(firstMouseClickEvent != null, "firstMouseClickEvent != null");
            Debug.Assert(barLostEvent != null, "barLostEvent != null");
            Debug.Assert(reachedPreGoalEvent != null, "reachedPreGoalEvent != null");
            Debug.Assert(stateMachine != null, "stateMachine != null");
        }
    }
}