using System;
using System.Collections;
using ItchyOwl.ObjectPooling;
using UnityEngine;

namespace RoofRails2
{
    public class RecycleAfterTime : MonoBehaviour
    {
        [SerializeField] private float timeAmount = 1f;
        private void Start()
        {
            StartCoroutine(RecycleAfter());
        }

        private IEnumerator RecycleAfter()
        {
            yield return new WaitForSeconds(timeAmount);

            gameObject.Recycle();
        }
    }
}