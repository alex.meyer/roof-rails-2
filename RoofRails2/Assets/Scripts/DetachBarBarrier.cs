using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace RoofRails2
{
    public class DetachBarBarrier : MonoBehaviour
    {
        [FormerlySerializedAs("fallenThroughBarsEvent")] [SerializeField] private EventSO detachBarEvent;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player"))
            {
                return;
            }

            detachBarEvent.Invoke();
        }

        private void ValidateReferences()
        {
            Debug.Assert(detachBarEvent != null, "fallenThroughBarsEvent != null");
        }
    }
}