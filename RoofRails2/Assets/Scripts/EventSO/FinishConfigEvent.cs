using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewFinishConfigEvent", menuName = "EventSO/FinishConfigEvent", order = 0)]
    public class FinishConfigEvent : EventSO
    {
        public FinishConfig finishConfig;
    }
}
