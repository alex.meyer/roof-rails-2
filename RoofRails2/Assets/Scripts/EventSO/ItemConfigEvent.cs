using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewItemConfigEvent", menuName = "EventSO/ItemConfigEvent", order = 0)]
    public class ItemConfigEvent : EventSO
    {
        public ItemConfig config;

        public void Invoke(ItemConfig itemConfig)
        {
            config = itemConfig;
            Invoke();
        }
    }
}