using UnityEngine;

namespace RoofRails2
{
    [CreateAssetMenu(fileName = "NewFloatEvent", menuName = "EventSO/FloatEvent", order = 0)]
    public class FloatEvent : EventSO
    {
        public float value;

        public void Invoke(float val)
        {
            value = val;
            Invoke();
        }
    }
}