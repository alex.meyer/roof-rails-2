using System;
using TMPro;
using UnityEngine;

namespace RoofRails2
{
    [Serializable]
    public class FadeAwayTextViewData
    {
        public string text;
    }
    public class FadeAwayTextViewController : MonoBehaviour
    {
        [SerializeField] private FadeAwayTextViewData data;
        [SerializeField] private TextMeshPro text;

        public FadeAwayTextViewData Data
        {
            get => data;
            set
            {
                data = value;
                RenderView();
            }
        }

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void RenderView()
        {
            text.text = data.text;
        }

        private void RegisterListeners() { }

        private void UnregisterListeners() { }

        private void ValidateReferences()
        {
            Debug.Assert(text != null, "text != null");
        }
    }
}